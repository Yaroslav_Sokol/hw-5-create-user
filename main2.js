// ВАРІАНТ 2

function createNewUser() {
    let firstName = prompt("Введіть своє ім'я:");
    let lastName = prompt("Введіть своє прізвище:");
    
    const newUser = {
      getLogin: function() {
        return (firstName.charAt(0) + lastName).toLowerCase();
      }
    };
    
    Object.defineProperties(newUser, 'firstName', {
        get: function() {
          return firstName;
        },
        set: function(value) {
          firstName = value;
        }
      }),

      Object.defineProperties(newUser, 'lastName', {
        get: function() {
          return lastName;
        },
        set: function(value) {
          lastName = value;
        }
      });
    
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());