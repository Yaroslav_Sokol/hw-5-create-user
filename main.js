// 1. Це така дія, яку можна виконати з об'єктом, викликавши його ім'ям та передаючи необхідні аргументи, які можуть буть використані у функції.
//    Наприклад, якщо ми маємо об'єкт "автомобіль" з властивостями "марка", "модель", "колір" то можемо додати до цього об'єкту метод "завести двигун", який буде виконувати певні дії з цим об'єктом, наприклад запускати двигун.
// 2. Властивості об'єкта може мати будь-який тип данних, наприклад: числа, рядки, иакож масисви, об'єкти, функції. Навіть "null" або "undefinde" може бути значенням властивості
// 3. Це означає, що змінні, які містять об'єкти, насправді містять посилання на об'єкт, а не його копію, тобо коли ми створюємо змінну, то насправді ми створюємо посилання на цей об'єкт у пам'яті, а не сам об'єкт.

// ВАРІАНТ 1

  function createNewUser() {
    let firstName = prompt("Введіть ім'я:");
    let lastName = prompt("Введіть прізвище:");
    
    const newUser = {
      getLogin: function() {
        return (firstName.charAt(0) + lastName).toLowerCase();
      }
    };
    
    Object.defineProperty(newUser, 'firstName', {
      get: function() {
        return firstName;
      },
    });
    
    Object.defineProperty(newUser, 'lastName', {
      get: function() {
        return lastName;
      },
    });
    
    // Дає можливість змінювати значення firstName та lastName відповідно, що дозволяє оновлювати значення властивостей
    newUser.setFirstName = function(value) {
      firstName = value;
    };
    
    newUser.setLastName = function(value) {
      lastName = value;
    };
    
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());
  
//   Також є другий варіант, він знаходиться в main2.js 

